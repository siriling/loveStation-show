# loveStation-show

[Chinese](https://gitee.com/siriling/loveStation-show/blob/master/README.md)|English

#### Description
基于Typecho情侣主题Brave，个人设计升级版

#### Demo

[Click](https://love.siriling.com:1314)

#### Software Architecture
PHP、JavaScript、CSS、HTML

#### Installation

1.  覆盖typecho的usr目录
2.  在网站后台设置为Brave主题
3.  按照教程设置独立页面（首页，祝福板，爱情清单，相册，爱情树，关于我们）
4.  在后台根据自己的需求启用插件

#### Instructions

获取源码：[点击获取](https://siriling.lanzoue.com/b03vezc1c)，密码：8qp4

请下载解压源码后查看里面的README.md文档

#### Function Show

[点击查看](https://gitee.com/siriling/loveStation-show/blob/master/show/show.md)


#### Gitee Feature

项目展示：https://gitee.com/siriling/loveStation-show

源码仓库：https://gitee.com/siriling/loveStation

Typecho源码：https://github.com/typecho

Brave主题源码：https://github.com/zwying0814/Brave

#### 赞助

获取源码**解压密码**请赞助40💰哦，然后请联系QQ1042585959

**希望还能给个祝福，谢谢了！**

<img src="images/mm_reward_qrcode_1672237420124.png" alt="mm_reward_qrcode_1672237420124" style="zoom: 33%;" />
