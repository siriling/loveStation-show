# 爱情小站-功能展示

# 一、主页

**6大基础功能：祝福板，点点滴滴，爱情清单，相册，爱情树，关于我们**

![image-20221228213034743](../images/image-20221228213034743.png)

**樱花飘落特效，音乐播放，右侧导航栏，彩色滚动条，多彩进度条，运行时间显示，备案号显示，底部滚动表白，底部鲸鱼动画，IP、地区和FPS显示**

![image-20221228213127506](../images/image-20221228213127506.png)

# 二、基础功能

### 1.祝福板

**祝福统计、评论时间、评论者IP、评论者系统和浏览器、表情包**

![image-20221228213735581](../images/image-20221228213735581.png)

![image-20221228220407823](../images/image-20221228220407823.png)

### 2.点点滴滴

![image-20221228213831887](../images/image-20221228213831887.png)

### 3.爱情清单

**未完成只显示文字、完成后显示图片和完成时间**

![image-20221228213930328](../images/image-20221228213930328.png)

![image-20221228214232708](../images/image-20221228214232708.png)

### 4.相册

**加密页面**

![image-20221228214336611](../images/image-20221228214336611.png)

**展示风格1**

![image-20221228214451677](../images/image-20221228214451677.png)

**展示风格2**

![image-20221228214708758](../images/image-20221228214708758.png)

### 5.爱情树

**相恋时间、动态的树效果、表白**

![image-20221228215018059](../images/image-20221228215018059.png)

**后台直接填写封装好的信息，更方便**

![image-20221228215858207](../images/image-20221228215858207.png)

### 6.关于我们

**提前设置好机器人，可进行自动对话**

![image-20221228215146630](../images/image-20221228215146630.png)

# 三、其他功能

#### 头部图片和背景图片

**头部视频设置、头部图片设置（可设置刷新换图）、背景图片设置（也可以设置刷新换图）**

![image-20221228215354208](../images/image-20221228215354208.png)

#### 音乐

**可根据自己需要设置歌曲、也可以使用别人创建好的歌单**

<img src="../images/image-20221228215622183.png" alt="image-20221228215622183" style="zoom:50%;" />

#### 底部信息

**备案号后台直接填写封装好的信息，更方便**

![image-20221228215958470](../images/image-20221228215958470.png)

#### 移动端底栏

<img src="../images/image-20221228220247874.png" alt="image-20221228220247874" style="zoom:50%;" />

**后台直接填写封装好的链接，更方便**

![image-20221228220111265](../images/image-20221228220111265.png)
